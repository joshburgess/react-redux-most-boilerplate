A generic boilerplate project for getting up and running with React (via create-react-app), Redux, React-Router, & the Redux-Most middleware. Also includes extra tools like Ramda, Creed (a Promise library from the creator of Most), AVA, etc., but they can easily be swapped out for your preferred tools.

## Instructions

```
npm install && npm start
```

or

```
yarn install && yarn start
```
