
import React from 'react'
import { connect } from 'react-redux'
import { getAppTitle } from '../../selectors'

const Header = ({ logo, title }) =>
  <header className='App-header'>
    <img src={logo} className='App-logo' alt='logo' />
    <h1 className='App-title'>{title}</h1>
  </header>

const mapStateToProps = state => ({
  title: getAppTitle(state),
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
