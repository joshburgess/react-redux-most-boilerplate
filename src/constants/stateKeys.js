export const APP = 'app'
export const IS_PINGING = 'isPinging'
export const PING_PONG_EXAMPLE = 'pingPongExample'
export const SOMETHING = 'something'
export const TITLE = 'title'
