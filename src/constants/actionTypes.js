// ping pong example
export const PING = 'PING'
export const PONG = 'PONG'

// main app
export const SET_SOMETHING = 'SET_SOMETHING'
