import test from 'ava'
import { unmountComponentAtNode } from 'react-dom'
import store from './store'
import mountApp from './utils/mountApp'
import configureStore from 'redux-mock-store'
import {
  SOMETHING,
  TITLE,
} from './constants/stateKeys'

/* eslint-disable */

const mockStore = configureStore()
const initialState = {
  [TITLE]: 'React-Redux-Most-Boilerplate Project',
  [SOMETHING]: '',
}

test('renders without crashing using a mock store', t => {
  const root = document.createElement('root')
  mountApp(mockStore(initialState))(root)
  unmountComponentAtNode(root)
  t.pass()
})

test('renders without crashing using the real store', t => {
  const root = document.createElement('root')
  mountApp(store)(root)
  unmountComponentAtNode(root)
  t.pass()
})

