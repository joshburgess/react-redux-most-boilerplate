import React from 'react'
import Message from '../Message'

const ErrorMessage = () =>
  <Message text='Sorry, there was an error.' />

export default ErrorMessage
