
import React from 'react'

const Message = ({ text }) =>
  <div className='App-intro-strip'>
    <h3 className='App-intro'>
      {text}
    </h3>
  </div>

export default Message
