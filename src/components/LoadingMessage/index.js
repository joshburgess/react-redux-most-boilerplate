import React from 'react'
import Message from '../Message'

const LoadingMessage = () =>
  <Message text='Loading...' />

export default LoadingMessage
