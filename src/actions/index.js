import {
  PING,
  PONG,
  SET_SOMETHING,
} from '../constants/actionTypes'
import {
  // IS_PINGING,
  SOMETHING,
} from '../constants/stateKeys'
// import compose from 'ramda/src/compose'
// import map from 'ramda/src/map'
// import mergeAll from 'ramda/src/mergeAll'
// import prop from 'ramda/src/prop'

export const ping = () => ({
  type: PING,
})

export const pong = () => ({
  type: PONG,
})

export const setSomething = something => ({
  type: SET_SOMETHING,
  payload: {
    [SOMETHING]: something,
  },
})
