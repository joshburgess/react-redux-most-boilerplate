import test from 'ava'
import {
  ping,
  pong,
  setSomething,
} from './'
import {
  PING,
  PONG,
  SET_SOMETHING,
} from '../constants/actionTypes'
import {
  SOMETHING,
} from '../constants/stateKeys'

/* eslint-disable */

test('ping properly creates a PING action', t => {
  const result = ping()
  const expected = {
    type: PING,
  }

  t.deepEqual(expected, result)
})

test('pong properly creates a PONG action', t => {
  const result = pong()
  const expected = {
    type: PONG,
  }

  t.deepEqual(expected, result)
})

test('setSomething properly creates a SET_SOMETHING action', t => {
  const result = setSomething(true)
  const expected = {
    type: SET_SOMETHING,
    payload: {
      [SOMETHING]: true,
    }
  }

  t.deepEqual(expected, result)
})
