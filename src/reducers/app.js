import {
  SET_SOMETHING,
} from '../constants/actionTypes'
import {
  TITLE,
  SOMETHING,
} from '../constants/stateKeys'
import {
  nextState,
  mergeSetSomething,
} from '../utils/reducer-utils'

const initialState = {
  [TITLE]: 'React-Redux-Most-Boilerplate Project',
  [SOMETHING]: '',
}

const app = (state = initialState, { type, payload }) => {
  const updates = {
    [SET_SOMETHING]: () => mergeSetSomething(state)(payload),
  }

  return nextState(state)(updates)(type)
}

export default app
