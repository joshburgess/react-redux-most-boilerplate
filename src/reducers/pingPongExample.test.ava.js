import test from 'ava'
import pingPongExampleReducer from './pingPongExample'
// import {
//   PING,
//   PONG,
// } from '../constants/actionTypes'
import {
  ping,
  pong,
} from '../actions'
import {
  IS_PINGING,
} from '../constants/stateKeys'

/* eslint-disable */

const initialState = {
  [IS_PINGING]: false,
}

test('pingPongExampleReducer should default to returning the initial state', t => {
  const result = pingPongExampleReducer(undefined, {})
  t.deepEqual(initialState, result)
})

test('pingPongExampleReducer should handle PING actions', t => {
  const result = pingPongExampleReducer(initialState, ping())
  const expected = {
    [IS_PINGING]: true,
  }
  t.deepEqual(expected, result)
})

test('pingPongExampleReducer should handle PONG actions', t => {
  const result = pingPongExampleReducer(initialState, pong())
  const expected = {
    [IS_PINGING]: false,
  }
  t.deepEqual(expected, result)
})

