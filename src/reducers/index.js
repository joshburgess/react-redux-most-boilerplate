import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import app from './app'
import pingPongExample from './pingPongExample'

const rootReducer = combineReducers({
  app,
  pingPongExample,
  routing: routerReducer,
})

export default rootReducer
