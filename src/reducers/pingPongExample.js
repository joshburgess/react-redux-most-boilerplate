import {
  PING,
  PONG,
} from '../constants/actionTypes'
import {
  IS_PINGING,
} from '../constants/stateKeys'
import {
  nextState,
  mergeIsPinging,
} from '../utils/reducer-utils'

const initialState = {
  [IS_PINGING]: false,
}

const pingPongExample = (state = initialState, { type, payload }) => {
  const updates = {
    [PING]: () => mergeIsPinging(state)(true),
    [PONG]: () => mergeIsPinging(state)(false),
  }

  return nextState(state)(updates)(type)
}

export default pingPongExample
