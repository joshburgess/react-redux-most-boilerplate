import createBrowserHistory from 'history/createBrowserHistory'
import createMemoryHistory from 'history/createMemoryHistory'
import { NODE_ENV } from '../constants'

const history = NODE_ENV === 'test'
  ? createMemoryHistory()
  : createBrowserHistory()

export default history
