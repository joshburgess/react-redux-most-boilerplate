import React from 'react'
import { Provider } from 'react-redux'
import './App.css'
import history from './history'
import { Route } from 'react-router'
import { ConnectedRouter } from 'react-router-redux'
import logo from './logo.svg'
import Header from './containers/Header'
// import Products from './containers/Products'
// import ProductDetails from './containers/ProductDetails'

const Home = () => <h1>Home</h1>
const SecondaryRoute = () => <h1>Secondary Route</h1>

const App = ({ store }) =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className='App'>
        <Header logo={logo} />
        <Route exact path='/' component={Home} />
        <Route exact path='/secondary' component={SecondaryRoute} />
      </div>
    </ConnectedRouter>
  </Provider>

export default App
