import { createSelector } from 'reselect'
import {
  APP,
  IS_PINGING,
  PING_PONG_EXAMPLE,
  TITLE,
} from '../constants/stateKeys'
// import compose from 'ramda/src/compose'
// import map from 'ramda/src/map'
import prop from 'ramda/src/prop'
// import uniq from 'ramda/src/uniq'
// import values from 'ramda/src/values'

export const getApp = state => prop(APP)(state)
export const getPingPongExample = state => prop(PING_PONG_EXAMPLE)(state)

// eliminate boilerplate for simple single prop get selectors
const createGetSelector = f => x => createSelector(
  [f],
  stateSubtree => prop(x)(stateSubtree)
)

// use currying to create reusable functions only needing a prop arg
const appSelect = createGetSelector(getApp)
const pingPongSelect = createGetSelector(getPingPongExample)

// simple get selectors
export const getAppTitle = appSelect(TITLE)
export const getIsPinging = pingPongSelect(IS_PINGING)
