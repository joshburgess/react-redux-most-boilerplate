import { createStore, applyMiddleware } from 'redux'
import compose from 'ramda/src/compose'
import { createEpicMiddleware } from 'redux-most'
import { createLogger } from 'redux-logger'
import { noOp } from '../utils'
import rootReducer from '../reducers'
import rootEpic from '../epics'
import { NODE_ENV } from '../constants'

const epicMiddleware = createEpicMiddleware(rootEpic)

const logger = createLogger({
  collapsed: true,
  diff: false,
  logErrors: true,
})

const devMiddleware = [
  logger,
  epicMiddleware,
]

const prodMiddleware = [
  epicMiddleware,
]

const middleware = NODE_ENV === 'development'
  ? devMiddleware
  : prodMiddleware

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // options here
    })
    : compose

const storeEnhancers = compose(
  composeEnhancers,
  applyMiddleware,
)(...middleware)

const store = createStore(rootReducer, storeEnhancers)

/******************************************************************************
  Start development only
*******************************************************************************/

/* eslint-disable fp/no-nil */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable better/explicit-return */
/* eslint-disable better/no-ifs */

// hot reload epics
const replaceRootEpic = () => {
  // commented out due to eslint not yet understanding dynamic imports
  // import('../epics').then(
  //   ({ default: nextRootEpic }) => { epicMiddleware.replaceEpic(nextRootEpic) }
  // )

  const nextRootEpic = require('../epics').default
  epicMiddleware.replaceEpic(nextRootEpic)
}

if (module.hot) {
  module.hot.accept('../epics', replaceRootEpic)
}

const globalStoreInDev = store => (
  store && NODE_ENV === 'development'
    ? () => { window.store = store } // eslint-disable-line fp/no-mutation
    : noOp
)()

// make store available globally, so we can dispatch from the console
globalStoreInDev(store) // eslint-disable-line fp/no-unused-expression

// hot reload reducers
const replaceRootReducer = () => {
  // commented out due to eslint not yet understanding dynamic imports
  // import('../reducers').then(
  //   ({ default: nextRootReducer }) => {
  //     store.replaceReducer(nextRootReducer)
  //     globalStoreInDev(store)
  //   }
  // )

  const nextRootReducer = require('../reducers').default
  store.replaceReducer(nextRootReducer)
}

if (module.hot) {
  module.hot.accept('../reducers', replaceRootReducer)
}

/* eslint-enable better/no-ifs */
/* eslint-enable better/explicit-return */
/* eslint-enable fp/no-unused-expression */
/* eslint-enable fp/no-nil */
/* eslint-enable fp/no-this */

/******************************************************************************
  End development only
*******************************************************************************/

export default store
