import { combineEpics } from 'redux-most'
import pingPongExample from './pingPongExample'

const rootEpic = combineEpics([
  pingPongExample,
])

export default rootEpic
