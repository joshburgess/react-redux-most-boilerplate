import { PING } from '../constants/actionTypes'
import { pong } from '../actions'
import { select } from 'redux-most'
import {
  curriedDelay as delay,
  curriedMap as map,
} from '../utils'
import compose from 'ramda/src/compose'

const pingPongExample = compose(
  map(pong),
  delay(1000),
  select(PING)
)

export default pingPongExample
