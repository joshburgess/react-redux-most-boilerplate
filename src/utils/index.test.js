import test from 'ava'
import {
  createError,
  fetchJsonStream,
  formatCurrency,
  getFakeResponseStream,
} from './'
import isObservable from 'is-observable'

/* eslint-disable */

test('fetchJsonStream returns an observable', t => { 
  const result = isObservable(fetchJsonStream('https://www.reddit.com/r/javascript'))

  t.true(result)
})

test('formatCurrency properly formats prices for the demo', t => { 
  const result = formatCurrency('24.9463')

  t.deepEqual(result, '$24.95')
})

test('createError returns an Error object', t => { 
  const result = createError('test')

  t.true(result instanceof Error)
})

test('getFakeResponseStream returns an observable', t => { 
  const result = getFakeResponseStream('anything')

  t.true(isObservable(result))
})