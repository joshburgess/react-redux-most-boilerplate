import React from 'react'
import { render } from 'react-dom'
import App from '../App'
// import { fetchProducts, fetchSchema } from '../actions'

/* eslint-disable fp/no-nil */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable better/explicit-return */
/* eslint-disable better/no-ifs */
const mountApp = store => rootEl => {
  const { dispatch } = store

  const renderCallback = () => {
    dispatch({ type: 'APP_INIT' })
    // dispatch(fetchProducts())
    // dispatch(fetchSchema())
  }

  const renderApp = Component => {
    render(<Component store={store} />, rootEl, renderCallback)
  }

  renderApp(App)

  /******************************************************************************
    Start development only
  *******************************************************************************/

  const replaceAppComponent = () => {
    // commented out due to eslint not yet understanding dynamic imports
    // import('../App').then(
    //   ({ default: NextApp }) => {
    //     render(<NextApp store={store} />, rootEl, renderCallback)
    //   }
    // )

    const NextApp = require('../App').default
    render(<NextApp store={store} />, rootEl, renderCallback)
  }

  if (module.hot) {
    module.hot.accept('../App', replaceAppComponent)
  }

  /* eslint-enable better/no-ifs */
  /* eslint-enable better/explicit-return */
  /* eslint-enable fp/no-unused-expression */
  /* eslint-enable fp/no-nil */
  /* eslint-enable fp/no-this */

  /******************************************************************************
    End development only
  *******************************************************************************/
}

export default mountApp
