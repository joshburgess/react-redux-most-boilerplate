import merge from 'ramda/src/merge'
import {
  IS_PINGING,
  SOMETHING,
} from '../../constants/stateKeys'
import prop from 'ramda/src/prop'

export const nextState = state => updates => type => {
  const fallback = () => state
  const update = prop(type)(updates)

  return (update || fallback)()
}

export const mergeIsPinging = state => boolVal =>
  merge(state)({
    [IS_PINGING]: boolVal || false,
  })

export const mergeSetSomething = state => payload =>
  merge(state)({
    [SOMETHING]: prop(SOMETHING)(payload) || false,
  })
