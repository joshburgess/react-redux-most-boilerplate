import compose from 'ramda/src/compose'
import curry from 'ramda/src/curry'
import not from 'ramda/src/not'
import prop from 'ramda/src/prop'

import {
  chain,
  debounce,
  delay,
  filter,
  fromPromise,
  map,
  merge,
  recoverWith,
  switchLatest,
  tap,
  take,
  throttle,
  until,
} from 'most'
import { delay as delayedResolve, reject } from 'creed'

/******************************************************************************
  General utilities
*******************************************************************************/

export const isArray = Array.isArray

export const isString = str => str &&
  (typeof str === 'string' || str instanceof String)

export const identity = f => f
// alias identity function as "no operation"
export const noOp = identity
export const log = console.log
// we could have used ramda's tap function here, but it was easy
// to implement without it
export const tapLog = x => log(x) || x
export const tapLogL = label => x => log(label, x) || x

// Here I would use localization/internationalization to format
// the currency (ex: the currency symbol, thousands separators, etc.),
// but this has been faked for the demo due to time constraints
// Note: fixing to 2 decimal places is only used for display purposes
// here and would not be done in a real situation dealing with currency
export const formatCurrency = value => `$${Number(value).toFixed(2)}`

// demonstrate how you could create a usable stream-based
// fetch utility with built-in http error handling
export const then = f => thenable => thenable.then(f)
export const toJson = response => response.json()
export const fetchJson = compose(then(toJson), fetch)
export const fetchJsonStream = compose(fromPromise, fetchJson)

// eslint-disable-next-line better/no-new
export const createError = msg => new Error(msg)

export const toJsonOrError = response => compose(not, prop('ok'))(response)
  ? reject(createError(prop('status')(response)))
  : toJson(response)

export const fetchJsonStreamWithHttpErrorHandling = compose(
  fromPromise,
  then(toJsonOrError),
  fetch,
)

export const delayedResolvedPromise = val => delayedResolve(1000, val)
export const getFakeResponseStream = compose(
  fromPromise,
  delayedResolvedPromise,
)

/******************************************************************************
  Stream utilities
*******************************************************************************/

const mapTo = (x, stream) => map(() => x, stream)
const switchMap = compose(switchLatest, map)

// prefix with "curried" so things are more obvious in other files
// Most 2.0 will make this no longer necessary, because it will feature
// an auto-curried API by default
export const curriedChain = curry(chain)
export const curriedDebounce = curry(debounce)
export const curriedDelay = curry(delay)
export const curriedFilter = curry(filter)
export const curriedMap = curry(map)
export const curriedMapTo = curry(mapTo)
export const curriedMerge = curry(merge)
export const curriedRecoverWith = curry(recoverWith)
export const curriedSwitchMap = curry(switchMap)
export const curriedTap = curry(tap)
export const curriedTake = curry(take)
export const curriedThrottle = curry(throttle)
export const curriedUntil = curry(until)
