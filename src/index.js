// Supply polyfills for older browsers
import 'babel-polyfill'
// Overwrite Promise implementation with Creed for better performance
import { shim } from 'creed'

import store from './store'
import mountApp from './utils/mountApp'
import './index.css'
// import { applyGlobalStyles } from './styles'
import registerServiceWorker from './registerServiceWorker'

const root = document.getElementById('root')

/* eslint-disable fp/no-unused-expression */
shim()
mountApp(store)(root)
// applyGlobalStyles()
registerServiceWorker()
/* eslint-enable fp/no-unused-expression */
